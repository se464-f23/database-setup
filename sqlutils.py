import os
import pymysql
from pymysql import Error


def connect():
    # Define Aurora MySQL connection details
    # Retrieve the values of the environment variables
    host = os.environ['RDS_HOSTNAME']
    user = os.environ['RDS_USERNAME']
    password = os.environ['RDS_PASSWORD']
    port = 3306
    database = os.environ['RDS_DATABASE']

    # Connect to database
    conn = pymysql.connect(host=host, port=port, user=user, password=password)
    cursor = conn.cursor()
    cursor.execute(f'CREATE DATABASE IF NOT EXISTS {database}')
    conn.select_db(database)
    return conn
